<?php

namespace App\Models;

interface Printable {

    public function printElement();
}
