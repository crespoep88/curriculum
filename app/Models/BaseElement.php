<?php

namespace App\Models;

class BaseElement
{
    protected $title;
    protected $description;
    protected $visible;

    public function __construct($title, $description)
    {
        $this->title = $title;
        $this->description = $description;
        $this->visible = true;
    }

    public function getTitle(){
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }



    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }



}

