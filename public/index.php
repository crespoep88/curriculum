<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Zend\Diactoros\ServerRequestFactory;

$capsule = new Capsule();

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'curriculum',
    'username'  => 'ezequiel',
    'password'  => 'acdc',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

// routing:
$request = ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

var_dump($request->getUri()->getPath());




if (isset($_GET['route']))
{
    if ($_GET['route'] != null)
    {
        if (($_GET['route'] == 'addJob'))
        {
            require '../addJob.php';
        } else {
            $route = $_GET['route'];
        }
    } else {
        require '../index.php';
    }
} else {
    require '../index.php';
}


